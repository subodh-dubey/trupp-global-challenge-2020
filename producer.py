import requests
import random
import json
from time import sleep

# to produce 20 random events and brodcast to API
def produce():
    events = []
    for e in range(20):
        types = ['INCREMENT','DECREMENT']
        type = random.choice(types)
        value = random.randint(1,5)
        data = {'type' : type, 'value': value}
        events.append(data)
        headers = {'content-type': 'application/json'}
        requests.post(url="http://127.0.0.1:5000/event",data=json.dumps(data),headers=headers)
        sleep(0.001)
    return events

if __name__ == '__main__':
    produce()


