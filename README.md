The application consists of 3 parts.
1. A REST API server 
2. A producer service that can broadcast random events to the API continuously 
3. A web UI that can display results as well as submit it's own events 

# 1. A REST API server 

I have build REST API server in Python using the Flask framework and MongoDB.   

**Flask** is a popular micro framework for building web applications. Since it is a micro-framework, it is very easy to use and lacks most of the advanced functionality which is found in a full-fledged framework.  

**MongoDB** is an open-source & document oriented NoSQL database. It uses JSON-like documents with schema. It is very powerful for quick & migration free development. To know more about MongoDB, check this very detailed tutorial. We will use MongoDB as our database & connect it with our flask web application.

## Setup
- Install Python 3.  
[Download and install python 3.](https://www.python.org/downloads/)
- Install MongoDb.  
[Download and install MongoDb](https://docs.mongodb.com/manual/administration/install-community/)

- Install all the required packages:  
Open terminal and run: 
```
pip install -r requirement.txt
```
- To start the Flask-RESTful server.
```
python app.py
```

- API Details:

| Method | Endpoint | RequiredParams | ResponseParams | Details |
| ------ | -------- | -------------- | --------------------- | ------------------------------------------------------------------------- |
| POST | /event | {     type: "INCREMENT",     value: 1   } | {     type: "INCREMENT",     value: 1   } | Records a new event on the server |
| GET | /event |  | [   {     type: "INCREMENT",     value: 1   },   {     type: "DECREMENT",     value: 3   },   {     type: "INCREMENT",     value: 5   } ] | Returns a JSON list of all events the server has received, in the order that they happened |
| GET | /value |  | integer value | Returns the current value of the system, which can be calculated by performing the actions described by all historical events |
| GET | /value/:t | | integer value | Instead of returning the latest value, this endpoint should return the value that the system had after the ﬁrst t events have happened. If the system has received no events, the value should be 0.|

# 2. A producer service that can broadcast random events to the API continuously 

To start the producer service, run the below command on terminal
```
python producer.py
```
or open url [http://127.0.0.1:5000/produce](http://127.0.0.1:5000/produce) 

# 3. A web UI that can display results as well as submit it's own events 

Open url [http://127.0.0.1:5000](http://127.0.0.1:5000) on your web browser to access web UI.

UI that allows us to:
1. View all historical events 
2. View the current value 
3. Add a new event
4. Produce 20 random events to the API
